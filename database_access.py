import datetime

import mysql.connector

mydb = mysql.connector.connect(
    # host="127.0.0.1",
    host="localhost",
    user="root",
    # password="Aa12345678!",
    # password="root",
    # password="Chien0001518",
    password="sa123456",
    database="DTV"
)

# print(mydb)
mycursor = mydb.cursor(buffered=True)


def select_all_user():
    mycursor.execute("SELECT * FROM USERS")
    myresult = mycursor.fetchall()
    # for result in myresult:
    #     print(result)
    return myresult


# insert

# sql = "INSERT INTO ROLES (name) VALUES (%s)"
# val = ["developer"]
# mycursor.execute(sql, val)
# mydb.commit()

def insert_case(violation_id, location, license_plate, image_id):
    created_date = datetime.datetime.now()
    value = [violation_id, location, license_plate, created_date, image_id]
    sql = "INSERT INTO UNCONFIRMED_CASES(violation_id, location, license_plate, created_date, image_id) VALUES (%s, %s, %s, %s, %s)"
    # sql = "INSERT INTO UNCONFIRMED_CASES(violation_id, location, license_plate, created_date, image_url) VALUES (%s, %s, %s, %s, %s)"
    print("INSERT THANH CONG RUI NE LOI: ", violation_id)
    mycursor.execute(sql, value)
    mydb.commit()
    case_id = mycursor.lastrowid
    return case_id


def insert_case_rate(violation_id, location, license_plate, image_id, rate):
    created_date = datetime.datetime.now()
    val = [violation_id, location, license_plate, created_date, image_id, rate]
    sql = "INSERT INTO UNCONFIRMED_CASES(violation_id, location, license_plate, created_date, image_id, rate) VALUES (%s, %s, %s, %s, %s, %s)"
    # sql = "INSERT INTO UNCONFIRMED_CASES(violation_id, location, license_plate, created_date, image_url) VALUES (%s, %s, %s, %s, %s)"

    mycursor.execute(sql, val)
    mydb.commit()


def insert_image(url, camera_id):
    sql = "INSERT INTO IMAGES(url, camera_id) VALUES (%s, %s)"
    val = [url, camera_id]
    mycursor.execute(sql, val)
    mydb.commit()
    image_id = mycursor.lastrowid
    return image_id


def retrieve_camera():
    mycursor.execute("SELECT * FROM CAMERAS")
    myresult = mycursor.fetchall()
    # for result in myresult:
    #     print(result[0])
    return myresult


def load_line(camera_id):
    result = []

    sql = "SELECT * FROM LINE WHERE CAMERA_ID = %s AND LINE_TYPE = %s"
    list_line = ["horizontal", "vertical", "upper_bound", "left_bound", "right_bound"]
    for line in list_line:
        val = [camera_id, line]
        mycursor.execute(sql, val)
        temp_result = mycursor.fetchall()
        # print(temp_result)
        result.append(temp_result)

    return result


def load_line_left(camera_id):
    result = []

    sql = "SELECT * FROM LINE WHERE CAMERA_ID = %s AND LINE_TYPE = %s"
    list_line = ["horizontal", "upper_bound", "left_bound", "right_bound"]
    for line in list_line:
        val = [camera_id, line]
        mycursor.execute(sql, val)
        temp_result = mycursor.fetchall()
        # print(temp_result)
        result.append(temp_result)

    return result


def load_location(camera_id):
    sql = "SELECT LOCATION FROM CAMERAS FROM CAMERA_ID = %s"
    val = [camera_id]
    mycursor.execute(sql, val)
    myresult = mycursor.fetchall()
    return myresult

# load_line(1)


# result = load_line(4)
# print(result[0][0][5])


# select_all_user()
# retrieve_camera()
