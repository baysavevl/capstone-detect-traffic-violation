import collections
import math
import sys
import random
import copy
import cv2

from utils.geometric_util import *
from test_package.print_frame import print_out_violation_picture, print_frame_process, save_img
from datetime import datetime
from utils.geometric_util import calculateOverlapRate


def dist(p1, p2):
    return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) * 2 + (p1.y - p2.y) * (p1.y - p2.y))


def dist_bottom(p1, p2):
    p1_x = (p1.left + p1.right) / 2
    p2_x = (p2.left + p2.right) / 2
    p1_y = p1.bottom
    p2_y = p2.bottom

    return math.sqrt((p1_x - p2_x) * (p1_x - p2_x) * 2.5 + (p1_y - p2_y) * (p1_y - p2_y) / 2)


def findClosest(moto, list):
    min_dis = sys.maxsize
    for iterator in list:
        min_dis = min(dist_bottom(moto, iterator), min_dis)
    return min_dis


def updateFrame(F1, F2):
    result = []

    if len(F2) == 0:
        return F1
    if len(F1) == 0:
        return F2

    # print("len lan luot :", len(F1), " - ", len(F2))
    ratio_bound = 1.15
    for loop1 in F1:
        current = updateMotorbike(loop1, loop1)
        temp = copy.copy(current)
        for next in F2:
            is_change = False
            cls_to_F1 = findClosest(current, F2)
            cls_to_F2 = findClosest(next, F1)
            if (cls_to_F1 == cls_to_F2) and (dist_bottom(current, next) == cls_to_F1):
                is_change = True
                if dist_bottom(current, next) > (min(calculateDistance(current), calculateDistance(next)) * ratio_bound):
                    is_change = False
                if (current.bottom < next.bottom) and (dist_bottom(current, next)>(current.right-current.left)/2):
                    is_change = False
            if is_change:
                temp = updateMotorbike(next, next)
                if temp.vio_pas_rate >= current.vio_pas_rate:
                    current.vio_pas_rate = temp.vio_pas_rate
                    current.img_pas = temp.img_pas

                    # update picture location
                    current.pas_l = temp.left
                    current.pas_r = temp.right
                    current.pas_t = temp.top
                    current.pas_b = temp.bottom

                    # print("update vio_pas_rate = ", current.vio_pas_rate)
                if temp.vio_cro_rate >= current.vio_cro_rate:
                    current.vio_cro_rate = temp.vio_cro_rate
                    current.img_cro = temp.img_cro

                    # update picture location
                    current.cro_l = temp.left
                    current.cro_r = temp.right
                    current.cro_t = temp.top
                    current.cro_b = temp.bottom

                if temp.vio_enc_rate >= current.vio_enc_rate:
                    current.vio_enc_rate = temp.vio_enc_rate
                    current.img_enc = temp.img_enc

                    # update picture location
                    current.enc_l = temp.left
                    current.enc_r = temp.right
                    current.enc_t = temp.top
                    current.enc_b = temp.bottom

                # tong hop lai diem vi pham
                current.vio_hel_rate = current.vio_hel_rate + temp.vio_hel_rate
                current.count_hel += 1
                if temp.vio_hel_rate == 100.0:
                    current.img_hel = temp.img_hel

                    # update picture location
                    current.hel_l = temp.left
                    current.hel_r = temp.right
                    current.hel_t = temp.top
                    current.hel_b = temp.bottom

                if temp.vio_wro_rate >= current.vio_wro_rate:
                    current.vio_wro_rate = temp.vio_wro_rate
                    current.img_wro = temp.img_wro

                    # update picture location
                    current.wro_l = temp.left
                    current.wro_r = temp.right
                    current.wro_t = temp.top
                    current.wro_b = temp.bottom

                current.top = copy.copy(temp.top)
                current.bottom = copy.copy(temp.bottom)
                current.left = copy.copy(temp.left)
                current.right = copy.copy(temp.right)
                current.x = copy.copy(temp.x)
                current.y = copy.copy(temp.y)
        result.append(current)

    for loop1 in F2:
        current = copy.copy(loop1)
        current = updateMotorbike(current, current)
        is_match = False
        for next in F1:
            cls_to_F1 = findClosest(current, F1)
            cls_to_F2 = findClosest(next, F2)
            if (cls_to_F1 == cls_to_F2) and (dist_bottom(current, next) == cls_to_F1):
                is_match = True
                if dist_bottom(current, next) > calculateDistance(current) * ratio_bound:
                    is_match = False
                if (current.bottom > next.bottom) and (dist_bottom(current, next) > (current.right-current.left)/2):
                    is_match = False
        # print("is_match = ", is_match)
        if not is_match:
            result.append(current)

    # print("total result before = ", len(result))
    count = -1
    final_res = []

    while count < len(result) - 1:
        count += 1
        mark = -1
        is_dup = False
        for iterator in range(0, len(final_res)-1):
            check_motor = final_res[iterator]
            if (result[count].x == check_motor.x) and (result[count].y == check_motor.y):
                if result[count].count_hel > check_motor.count_hel:
                    mark = iterator
                is_dup = True
        if not is_dup:
            final_res.append(result[count])
        if mark != -1:
            final_res[mark] = result[count]
    # print("total result after = ", len(final_res))

    return final_res


def process(LF):
    LF = collections.OrderedDict(sorted(LF.items()))
    print("len = ", len(LF))

    if len(LF) == 1:
        return LF[1]
    else:
        current_frame = LF[1]
        for frame in range(1, (len(LF)) + 1):
            print("Frame number ", frame)
            current_frame = updateFrame(current_frame, LF[frame])
            print_frame_process(current_frame)

        for moto in current_frame:
            moto.vio_hel_rate = moto.vio_hel_rate / (moto.count_hel + 1)

        return current_frame


def process_v3(locations):
    result = []
    list_location = []
    tmp_location = locations.copy()
    if len(tmp_location) > 1:
        for i in range(1, len(tmp_location)):
            violations = tmp_location[i]            
            for motorbike in violations: 
                is_existed = False
                for j in range(0, i):
                    pre_violations = tmp_location[j]                                   
                    for pre_motorbike in pre_violations:
                        if pre_motorbike.id == motorbike.id:
                            is_existed = True
                            break
                    if is_existed == True:
                        break
                if is_existed == False:
                    motorbike_id = ''
                    for k in range(0, i):
                        pre_violations = tmp_location[k]
                        for pre_motorbike in pre_violations:
                            overlapRate = calculateOverlapRate(motorbike, pre_motorbike)
                            if overlapRate >= 0.15:
                                is_next = False
                                for l in range((k + 1), len(tmp_location)):
                                    next_violations = tmp_location[l]
                                    for next_motorbike in next_violations:
                                        if next_motorbike.id == pre_motorbike.id:
                                            is_next = True
                                            break
                                    if is_next == True:
                                        break
                                if is_next == False:
                                    motorbike_id = pre_motorbike.id
                                    for m in range(i, len(tmp_location)):
                                        current_violations = tmp_location[m]
                                        for current_motorbike in current_violations:
                                            if current_motorbike.id == motorbike.id:
                                                current_motorbike.id = motorbike_id
                        if motorbike_id != '':
                            break
    for violations in tmp_location:
        for motorbike in violations:
            list_location.append(motorbike)
    while len(list_location) > 0:
        tmp_motorbike = list_location[0]
        total_helmet = 0
        helmet_violation = 0
        motorbikes = list_location.copy()
        for motorbike in motorbikes:
            if tmp_motorbike.id == motorbike.id:
                total_helmet += 1                    
                if tmp_motorbike.vio_pas_rate < motorbike.vio_pas_rate:
                        tmp_motorbike.vio_pas_rate = motorbike.vio_pas_rate
                        tmp_motorbike.img_pas = motorbike.img_pas
                        tmp_motorbike.pas_l = motorbike.pas_l
                        tmp_motorbike.pas_t = motorbike.pas_t
                        tmp_motorbike.pas_r = motorbike.pas_r
                        tmp_motorbike.pas_b = motorbike.pas_b
                        tmp_motorbike.pas_lc = motorbike.pas_lc
                
                if tmp_motorbike.vio_pas_rate == 0:
                    if tmp_motorbike.vio_cro_rate < motorbike.vio_cro_rate:
                        tmp_motorbike.vio_cro_rate = motorbike.vio_cro_rate
                        tmp_motorbike.img_cro = motorbike.img_cro
                        tmp_motorbike.cro_l = motorbike.cro_l
                        tmp_motorbike.cro_t = motorbike.cro_t
                        tmp_motorbike.cro_r = motorbike.cro_r
                        tmp_motorbike.cro_b = motorbike.cro_b
                        tmp_motorbike.cro_lc = motorbike.cro_lc

                if tmp_motorbike.vio_enc_rate < motorbike.vio_enc_rate:
                    tmp_motorbike.vio_enc_rate = motorbike.vio_enc_rate
                    tmp_motorbike.img_enc = motorbike.img_enc
                    tmp_motorbike.enc_l = motorbike.enc_l
                    tmp_motorbike.enc_t = motorbike.enc_t
                    tmp_motorbike.enc_r = motorbike.enc_r
                    tmp_motorbike.enc_b = motorbike.enc_b
                    tmp_motorbike.enc_lc = motorbike.enc_lc

                is_added = False
                if motorbike.vio_hel_rate == 100:
                    helmet_violation += 1
                    img_hel = cv2.imread(motorbike.img_hel)
                    img = copy.copy(img_hel)
                    img = img[int(motorbike.hel_t):int(motorbike.hel_b), int(motorbike.hel_l):int(motorbike.hel_r)]
                    img = img[int(motorbike.hel_lc.top):int(motorbike.hel_lc.bottom), int(motorbike.hel_lc.left):int(motorbike.hel_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                    is_added = True
                if motorbike.vio_pas_rate > 0 and is_added == False:
                    img_pas = cv2.imread(motorbike.img_pas)
                    img = copy.copy(img_pas)
                    img = img[int(motorbike.pas_t):int(motorbike.pas_b), int(motorbike.pas_l):int(motorbike.pas_r)]
                    img = img[int(motorbike.pas_lc.top):int(motorbike.pas_lc.bottom), int(motorbike.pas_lc.left):int(motorbike.pas_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                    is_added = True
                if motorbike.vio_cro_rate > 0 and is_added == False:
                    img_cro = cv2.imread(motorbike.img_cro)
                    img = copy.copy(img_cro)
                    img = img[int(motorbike.cro_t):int(motorbike.cro_b), int(motorbike.cro_l):int(motorbike.cro_r)]
                    img = img[int(motorbike.cro_lc.top):int(motorbike.cro_lc.bottom), int(motorbike.cro_lc.left):int(motorbike.cro_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                    is_added = True
                if motorbike.vio_enc_rate > 0 and is_added == False: 
                    img_enc = cv2.imread(motorbike.img_enc)
                    img = copy.copy(img_enc)
                    img = img[int(motorbike.enc_t):int(motorbike.enc_b), int(motorbike.enc_l):int(motorbike.enc_r)]
                    img = img[int(motorbike.enc_lc.top):int(motorbike.enc_lc.bottom), int(motorbike.enc_lc.left):int(motorbike.enc_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                list_location.remove(motorbike)
            
        tmp_motorbike.vio_hel_rate = helmet_violation / total_helmet * 100
        result.append(tmp_motorbike)
    return result


def process_v2(list_location):
    result = []
    while len(list_location) > 0:
        tmp_motorbike = list_location[0]
        total_helmet = 0
        helmet_violation = 0
        motorbikes = list_location.copy()
        for motorbike in motorbikes:
            if tmp_motorbike.id == motorbike.id:
                total_helmet += 1                    
                if tmp_motorbike.vio_pas_rate < motorbike.vio_pas_rate:
                        tmp_motorbike.vio_pas_rate = motorbike.vio_pas_rate
                        tmp_motorbike.img_pas = motorbike.img_pas
                        tmp_motorbike.pas_l = motorbike.pas_l
                        tmp_motorbike.pas_t = motorbike.pas_t
                        tmp_motorbike.pas_r = motorbike.pas_r
                        tmp_motorbike.pas_b = motorbike.pas_b
                        tmp_motorbike.pas_lc = motorbike.pas_lc
                
                if tmp_motorbike.vio_pas_rate == 0:
                    if tmp_motorbike.vio_cro_rate < motorbike.vio_cro_rate:
                        tmp_motorbike.vio_cro_rate = motorbike.vio_cro_rate
                        tmp_motorbike.img_cro = motorbike.img_cro
                        tmp_motorbike.cro_l = motorbike.cro_l
                        tmp_motorbike.cro_t = motorbike.cro_t
                        tmp_motorbike.cro_r = motorbike.cro_r
                        tmp_motorbike.cro_b = motorbike.cro_b
                        tmp_motorbike.cro_lc = motorbike.cro_lc

                if tmp_motorbike.vio_enc_rate < motorbike.vio_enc_rate:
                    tmp_motorbike.vio_enc_rate = motorbike.vio_enc_rate
                    tmp_motorbike.img_enc = motorbike.img_enc
                    tmp_motorbike.enc_l = motorbike.enc_l
                    tmp_motorbike.enc_t = motorbike.enc_t
                    tmp_motorbike.enc_r = motorbike.enc_r
                    tmp_motorbike.enc_b = motorbike.enc_b
                    tmp_motorbike.enc_lc = motorbike.enc_lc

                if motorbike.vio_hel_rate == 100:
                    helmet_violation += 1
                    img_hel = cv2.imread(motorbike.img_hel)
                    img = copy.copy(img_hel)
                    img = img[int(motorbike.hel_t):int(motorbike.hel_b), int(motorbike.hel_l):int(motorbike.hel_r)]
                    img = img[int(motorbike.hel_lc.top):int(motorbike.hel_lc.bottom), int(motorbike.hel_lc.left):int(motorbike.hel_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                if motorbike.vio_pas_rate > 0:
                    img_pas = cv2.imread(motorbike.img_pas)
                    img = copy.copy(img_pas)
                    img = img[int(motorbike.pas_t):int(motorbike.pas_b), int(motorbike.pas_l):int(motorbike.pas_r)]
                    img = img[int(motorbike.pas_lc.top):int(motorbike.pas_lc.bottom), int(motorbike.pas_lc.left):int(motorbike.pas_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                if motorbike.vio_cro_rate > 0:
                    img_cro = cv2.imread(motorbike.img_cro)
                    img = copy.copy(img_cro)
                    img = img[int(motorbike.cro_t):int(motorbike.cro_b), int(motorbike.cro_l):int(motorbike.cro_r)]
                    img = img[int(motorbike.cro_lc.top):int(motorbike.cro_lc.bottom), int(motorbike.cro_lc.left):int(motorbike.cro_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                if motorbike.vio_enc_rate > 0: 
                    img_enc = cv2.imread(motorbike.img_enc)
                    img = copy.copy(img_enc)
                    img = img[int(motorbike.enc_t):int(motorbike.enc_b), int(motorbike.enc_l):int(motorbike.enc_r)]
                    img = img[int(motorbike.enc_lc.top):int(motorbike.enc_lc.bottom), int(motorbike.enc_lc.left):int(motorbike.enc_lc.right)]
                    path = save_img(random.randint(1,99), img, 'license')
                    tmp_motorbike.imgs.append(path)
                list_location.remove(motorbike)
            
        tmp_motorbike.vio_hel_rate = helmet_violation / total_helmet * 100
        result.append(tmp_motorbike)
    # for motorbike in result:
    #     tmp = 0
    #     for img in motorbike.imgs:
    #         tmp += 1
    #         save_img(tmp, img)
    return result



def updateMotorbike(motorbike, update):
    temp = copy.copy(motorbike)
    # print("toa do ne : ", update.left, " - ", update.right, " - ", update.top, " - ", update.bottom)

    temp.img_cro = update.img_cro
    temp.img_enc = update.img_enc
    temp.img_pas = update.img_pas
    temp.img_hel = update.img_hel
    temp.img_wro = update.img_wro

    temp.pas_l = update.left
    temp.cro_l = update.left
    temp.enc_l = update.left
    temp.hel_l = update.left
    temp.wro_l = update.left

    temp.pas_r = update.right
    temp.cro_r = update.right
    temp.enc_r = update.right
    temp.hel_r = update.right
    temp.wro_r = update.right

    temp.pas_t = update.top
    temp.cro_t = update.top
    temp.enc_t = update.top
    temp.hel_t = update.top
    temp.wro_t = update.top

    temp.pas_b = update.bottom
    temp.cro_b = update.bottom
    temp.enc_b = update.bottom
    temp.hel_b = update.bottom
    temp.wro_b = update.bottom

    return temp
