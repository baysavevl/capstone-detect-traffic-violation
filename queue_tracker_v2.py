import threading
import time
import copy
import tensorflow as tf
import numpy as np
import math
import cv2

from numpy import *
from deep_sort import nn_matching
from deep_sort import preprocessing
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
from deep_sort import generate_detections as gdet
from tensorflow.python.saved_model import tag_constants
from multiprocessing import Process, Queue
from datetime import datetime

from aggregateViolations import process_v3
from case_resolve import conduct_case_v2
from service.draw import draw_line, draw
from service.logic import *
from location.TrafficLight import TrafficLight
from test_package.print_frame import print_out_violation_picture, print_frame_process, print_raw_frame, print_out_violation_picture_light, print_detect_frame, save_img, delete_img
from yolov3.configs import *
from yolov3.yolov4 import *
from yolov3.utils import detect_image, Load_Yolo_model, image_preprocess, nms, postprocess_boxes, draw_bbox, is_red
from thread_test import light_status
from database_access import load_line, load_line_left, retrieve_camera
import sys
import pygame

pygame.init()

def end_game():
    pygame.quit()
    sys.exit(0)    # Use sys.exit, not os._exit

clock = pygame.time.Clock()

start_time = pygame.time.get_ticks()   # Time in milliseconds
stop_after = 3600 * 1000

yolo = Load_Yolo_model()
countRed = 0
countGreen = 0
totalFrame = 0
startGreenFrame = 0
startRedFrame = 0
redFrame = 0
green_frame = 0
listRedPosition = []
listGreenPosition = []
original_frames = Queue()
original_frames_left = Queue()
images_queue = Queue()
images_queue_left = Queue()
last_green = []
last_helmet_insert = []
last_wro_insert = []



def get_time():
    now = datetime.datetime.now()
    dt_string = now.strftime("%d:%m:%Y %H:%M:%S")
    return dt_string


def increment(name):
    global countRed
    global countGreen
    global totalFrame
    global redFrame
    global green_frame

    if name == 'countRed':
        countRed += 1
    elif name == 'countGreen':
        countGreen += 1
    elif name == 'totalFrame':
        totalFrame += 1
    elif name == 'redFrame':
        redFrame += 1
    elif name == 'green_frame':
        green_frame += 1


def detect_image_left(Yolo, input_size=416, CLASSES=YOLO_COCO_CLASSES, score_threshold=0.4, iou_threshold=0.35, rectangle_colors='', helmet_flag=True, traffic_flag=True, camera_id=1, location='', lower_bound=LinePoint(0, 0, 0, 0), list_bounding=[]):
    global original_frames_left
    global last_helmet_insert
    global last_wro_insert
    Track_only = ["motorbike"]
    max_cosine_distance = 0.6
    nn_budget = 1000
    nms_max_overlap = 1.0
    model_filename = 'D:/FPT_Semester/Capstone_Project/yolov4-model-server/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    metric = nn_matching.NearestNeighborDistanceMetric(
        "cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric)
    while True:
        if original_frames_left.qsize() > 0:
            path = original_frames_left.get()
            image = cv2.imread(path)
            original_image = copy.copy(image)
            image_data = image_preprocess(np.copy(original_image), [416, 416])
            image_data = image_data[np.newaxis, ...].astype(np.float32)
            if YOLO_FRAMEWORK == "tf":
                print(' ')
                print('Start time for frame detection: ' + str(get_time()))
                NUM_CLASS = read_class_names(CLASSES)
                key_list = list(NUM_CLASS.keys())
                val_list = list(NUM_CLASS.values())

                pred_bbox = []
                pred_bbox = Yolo.predict(image_data)
                pred_bbox = [tf.reshape(x, (-1, tf.shape(x)[-1]))
                             for x in pred_bbox]
                pred_bbox = tf.concat(pred_bbox, axis=0)
                bboxes = postprocess_boxes(
                    pred_bbox, original_image, input_size, score_threshold)
                bboxes = nms(bboxes, iou_threshold, method='nms')

                # start tracking
                boxes, scores, names = [], [], []
                for bbox in bboxes:
                    if len(Track_only) != 0 and NUM_CLASS[int(bbox[5])] in Track_only or len(Track_only) == 0:
                        boxes.append([bbox[0].astype(int), bbox[1].astype(int), bbox[2].astype(
                            int)-bbox[0].astype(int), bbox[3].astype(int)-bbox[1].astype(int)])
                        # scores.append(bbox[4])
                        scores.append(1)
                        names.append(NUM_CLASS[int(bbox[5])])
                features = np.array(encoder(copy.copy(original_image), boxes))
                detections = [Detection(bbox, score, class_name, feature) for bbox,
                              score, class_name, feature in zip(boxes, scores, names, features)]
                boxs = np.array([d.tlwh for d in detections])
                scores = np.array([d.confidence for d in detections])
                classes = np.array([d.class_name for d in detections])
                indices = preprocessing.non_max_suppression(
                    boxs, classes, nms_max_overlap, scores)
                detections = [detections[i] for i in indices]
                tracker.predict()
                tracker.update(detections)
                tracked_bboxes = []
                for track in tracker.tracks:
                    if not track.is_confirmed() or track.time_since_update > 1:
                        continue
                    bbox = track.to_tlbr()
                    class_name = track.get_class()
                    tracking_id = track.track_id
                    index = key_list[val_list.index(class_name)]
                    tracked_bboxes.append(bbox.tolist() + [tracking_id, index])
                tempList = draw_bbox(
                    original_image, tracked_bboxes, CLASSES=CLASSES, rectangle_colors=rectangle_colors, tracking=True, helmet_flag=helmet_flag,
                    traffic_flag=traffic_flag, upperBound=list_bounding[0], leftBound=list_bounding[1],
                    rightBound=list_bounding[2], lower_bound=lower_bound)
                pboxes = []
                for bbox in bboxes:
                    if 'person' == NUM_CLASS[int(bbox[5])]:
                        pboxes.append(bbox)
                people_list = draw_bbox(
                    original_image, pboxes, CLASSES=CLASSES, rectangle_colors=rectangle_colors, helmet_flag=False,
                    traffic_flag=False)['PersonLocation']
                tempList['PersonLocation'] = people_list
                motorbike_list = tempList['MotorbikeLocation']
                helmet_list = tempList['HelmetLocation']
                people_list = tempList['PersonLocation']
                storage_frame = copy.copy(original_image)
                '''if len(motorbike_list) > 0:
                    for i in motorbike_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='')
                if len(helmet_list) > 0:
                    for i in helmet_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='Helmet')
                if len(people_list) > 0:
                    for i in people_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='Person')
                '''
                violations = detect_left_violation_v2(
                        tempList, path, last_helmet_insert, last_wro_insert)
                if len(violations) > 0:
                    print_out_violation_picture_light(
                            violations, list_bounding, storage_frame, True)
                    print(' ')
                    print("=============== FINAL RESULT =============")
                    for motorbike in violations:
                        print("Motorbike ID: ", motorbike.id)
                        print("Wrong lane rate = ", motorbike.vio_wro_rate)
                        conduct_case_v2(
                            camera_id, motorbike, location, '', lower_bound, list_bounding)
                        if motorbike.vio_wro_rate > 0:
                            last_wro_insert.append(motorbike.id)
                        if motorbike.vio_hel_rate > 0:
                            last_helmet_insert.append(motorbike.id)
                        print(' ')
                print('Finish time for frame detection: ' + str(get_time()))
                print('===================================')


def detect_image(Yolo, input_size=416, CLASSES=YOLO_COCO_CLASSES, score_threshold=0.4, iou_threshold=0.35, rectangle_colors='', helmet_flag=True, traffic_flag=True, camera_id=1, location='', verticalLine=LinePoint(0, 0, 0, 0), horizontalLine=LinePoint(0, 0, 0, 0), list_bounding=[]):
    global listRedPosition
    global countRed
    global countGreen
    global listGreenPosition
    global totalFrame
    global startGreenFrame
    global startRedFrame
    global redFrame
    global green_frame
    global original_frames
    global last_green
    Track_only = ["motorbike"]
    max_cosine_distance = 0.6
    nn_budget = 1000
    nms_max_overlap = 1.0
    model_filename = 'D:/FPT_Semester/Capstone_Project/yolov4-model-server/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
    metric = nn_matching.NearestNeighborDistanceMetric(
        "cosine", max_cosine_distance, nn_budget)
    tracker = Tracker(metric)
    while True:
        if original_frames.qsize() > 0:
            path = original_frames.get()
            image = cv2.imread(path)
            original_image = copy.copy(image)
           
            image_data = image_preprocess(np.copy(original_image), [416, 416])
            image_data = image_data[np.newaxis, ...].astype(np.float32)
            if YOLO_FRAMEWORK == "tf":
                increment('totalFrame')
                print('')
                print('Current frame: ' + str(totalFrame))
                print('Start time for frame detection: ' + str(get_time()))
                NUM_CLASS = read_class_names(CLASSES)
                key_list = list(NUM_CLASS.keys())
                val_list = list(NUM_CLASS.values())

                pred_bbox = []                
                pred_bbox = Yolo.predict(image_data)
                pred_bbox = [tf.reshape(x, (-1, tf.shape(x)[-1]))
                             for x in pred_bbox]
                pred_bbox = tf.concat(pred_bbox, axis=0)
                bboxes = postprocess_boxes(
                    pred_bbox, original_image, input_size, score_threshold)
                bboxes = nms(bboxes, iou_threshold, method='nms')

                # start tracking
                boxes, scores, names = [], [], []
                for bbox in bboxes:
                    if len(Track_only) != 0 and NUM_CLASS[int(bbox[5])] in Track_only or len(Track_only) == 0:
                        boxes.append([bbox[0].astype(int), bbox[1].astype(int), bbox[2].astype(
                            int)-bbox[0].astype(int), bbox[3].astype(int)-bbox[1].astype(int)])
                        # scores.append(bbox[4])
                        scores.append(1)
                        names.append(NUM_CLASS[int(bbox[5])])
                features = np.array(encoder(copy.copy(original_image), boxes))
                detections = [Detection(bbox, score, class_name, feature) for bbox,
                              score, class_name, feature in zip(boxes, scores, names, features)]
                boxs = np.array([d.tlwh for d in detections])
                scores = np.array([d.confidence for d in detections])
                classes = np.array([d.class_name for d in detections])
                indices = preprocessing.non_max_suppression(
                    boxs, classes, nms_max_overlap, scores)
                detections = [detections[i] for i in indices]
                tracker.predict()
                tracker.update(detections)
                tracked_bboxes = []
                for track in tracker.tracks:
                    if not track.is_confirmed() or track.time_since_update > 1:
                        continue
                    bbox = track.to_tlbr()
                    class_name = track.get_class()
                    tracking_id = track.track_id
                    index = key_list[val_list.index(class_name)]
                    tracked_bboxes.append(bbox.tolist() + [tracking_id, index])
                tempList = draw_bbox(
                    original_image, tracked_bboxes, CLASSES=CLASSES, rectangle_colors=rectangle_colors, tracking=True, helmet_flag=helmet_flag,
                    traffic_flag=traffic_flag, upperBound=list_bounding[0], leftBound=list_bounding[1],
                    rightBound=list_bounding[2])
                pboxes = []
                for bbox in bboxes:
                    if 'person' == NUM_CLASS[int(bbox[5])]:
                        pboxes.append(bbox)
                people_list = draw_bbox(
                    original_image, pboxes, CLASSES=CLASSES, rectangle_colors=rectangle_colors, helmet_flag=False,
                    traffic_flag=False)['PersonLocation']
                tempList['PersonLocation'] = people_list
                traffic_light = tempList['TrafficLight']
                motorbike_list = tempList['MotorbikeLocation']
                helmet_list = tempList['HelmetLocation']
                people_list = tempList['PersonLocation']
                if len(traffic_light) == 0:
                    traffic_light.append(TrafficLight(
                        int(1727), int(269), int(1801), int(393),
                        is_red(int(1727), int(269), int(1801), int(393), original_image)))

                    # traffic_light.append(TrafficLight(
                    #                     int(1600), int(285), int(1660), int(405),
                    #                     is_red(int(1600), int(285), int(1660), int(405), original_image)))
                storage_frame = copy.copy(original_image)
                
                '''if len(motorbike_list) > 0:
                    for i in motorbike_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='')
                # print_detect_frame(storage_frame)
                '''
                if len(traffic_light) > 0:
                    for i in traffic_light:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='Traffic light')
                
                '''if len(helmet_list) > 0:
                    for i in helmet_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='Helmet')
                if len(people_list) > 0:
                    for i in people_list:
                        storage_frame = draw(
                            storage_frame, i.left, i.top, i.right, i.bottom, text='Person')
                '''
                red = light_status(traffic_light)
                if red:
                    increment('countRed')
                    if countRed == 1:
                        redFrame = totalFrame
                        startRedFrame = totalFrame
                    else:
                        increment('redFrame')
                    if countGreen < 11:
                        if countGreen > 0 and countRed > 10:
                            len_tmp = len(listGreenPosition)
                            for j in range(0, countGreen):
                                increment('countRed')
                                increment('redFrame')
                            if len_tmp > 0:
                                for violation in listGreenPosition:
                                    listRedPosition.append(violation)
                                listGreenPosition.clear()
                            countGreen = 0
                            green_frame = 0
                            startGreenFrame = 0
                            green_len = 0
                    violations = detect_right_violation_v2(
                        tempList, horizontalLine, list_bounding[0], verticalLine, path, list_bounding[1], list_bounding[2], last_green)
                    if len(violations) > 0:
                        print_out_violation_picture_light(
                            violations, list_bounding, storage_frame, red)
                        '''for motorbike in violations:
                            listRedPosition.append(motorbike)
                        '''
                        listRedPosition.append(violations)
                else:
                    increment('countGreen')
                    last_green.clear()
                    if countGreen == 1:
                        green_frame = totalFrame
                        startGreenFrame = totalFrame
                    else:
                        increment('green_frame')
                    if countRed < 11:
                        if countRed > 0 and countGreen > 10:
                            len_tmp = len(listRedPosition)
                            for j in range(0, countRed):
                                increment('countGreen')
                                increment('green_frame')
                            if len_tmp > 0:
                                for violation in listRedPosition:
                                    for motorbike in violation:
                                        if motorbike.vio_cro_rate > 0:
                                            motorbike.vio_cro_rate = 0
                                            motorbike.img_cro = ''
                                            motorbike.cro_lc = ''
                                            motorbike.cro_l = ''
                                            motorbike.cro_t = ''
                                            motorbike.cro_r = ''
                                            motorbike.cro_b = ''
                                        if motorbike.vio_pas_rate > 0:
                                            motorbike.vio_pas_rate = 0
                                            motorbike.img_pas = ''
                                            motorbike.pas_lc = ''
                                            motorbike.pas_l = ''
                                            motorbike.pas_t = ''
                                            motorbike.pas_r = ''
                                            motorbike.pas_b = ''
                                    listGreenPosition.append(violation)
                                listRedPosition.clear()
                            countRed = 0
                            redFrame = 0
                            startRedFrame = 0
                            red_len = 0
                    violations = detect_right_green_violation_v2(
                        tempList, horizontalLine, list_bounding[0], verticalLine, path, list_bounding[1], list_bounding[2])
                    if len(violations) > 0:
                        print_out_violation_picture_light(
                            violations, list_bounding, storage_frame, red)
                        for motorbike in violations:
                            if motorbike.vio_pas_rate > 0:
                                last_green.append(motorbike.id)
                                motorbike.vio_pas_rate = 0
                        listGreenPosition.append(violations)
                final_process = []
                if startGreenFrame == (redFrame + 1) and countGreen > 10 and redFrame != 0:
                    if len(listRedPosition) != 0:
                        final_process = process_v3(listRedPosition)
                        listRedPosition.clear()
                    countRed = 0
                    totalFrame = green_frame
                    redFrame = 0
                    startGreenFrame = 0
                    startRedFrame = 0
                if startRedFrame == (green_frame + 1) and countRed > 10 and green_frame != 0:
                    if len(listGreenPosition) != 0:
                        final_process = process_v3(listGreenPosition)
                        listGreenPosition.clear() 
                    countGreen = 0
                    totalFrame = redFrame
                    green_frame == 0
                    startRedFrame = 0
                    startGreenFrame = 0
                if len(final_process) > 0:
                    print(" ")
                    print("=================== FINAL RESULT ================")
                    for object in final_process:
                        print("Motorbike ID: ", object.id)
                        print("Passing red light rate = ", object.vio_pas_rate)
                        print("Crossing rate = ", object.vio_cro_rate)
                        print("Encroaching rate = ", object.vio_enc_rate)
                        print("Not wearing helmet rate = ", object.vio_hel_rate)
                        conduct_case_v2(
                            camera_id, object, location, verticalLine, horizontalLine, list_bounding)
                        print(' ')
                print('Finish time for frame detection: ' + str(get_time()))
                print('===============================')


def write_images():
    count = 0
    while True:
        if images_queue.qsize() > 0:
            try:
                count += 1
                image = images_queue.get()
                path = save_img(count, image, 'queue-right')
                original_frames.put(path)
                print('Wrote ' + str(count) + ' images')
            except:
                pass


def write_images_left():
    count = 0
    while True:
        if images_queue_left.qsize() > 0:
            try:
                count += 1                
                image = images_queue_left.get()
                path = save_img(count, image, 'queue-left')
                original_frames_left.put(path)
                print('Wrote ' + str(count) + ' images')
            except:
                pass


def process_each_frame(camera):
    camera_id = camera[0]
    location = camera[1]
    connection = camera[2]
    status = camera[3]
    position = camera[4]
    global totalFrame
    if status == "active":
        cap = cv2.VideoCapture(connection)
        # cap = cv2.VideoCapture('rtsp://admin:palexy802@192.168.43.9:5544/Streaming/channels/101')
        fps = math.floor(int(cap.get(cv2.CAP_PROP_FPS)) / 7)
        line_list = load_line(camera_id)
        count = 0
        if position == 'right':
            horizontalLine = LinePoint(line_list[0][0][3], line_list[0][0][2], line_list[0][0][4], line_list[0][0][5])
            verticalLine = LinePoint(line_list[1][0][3], line_list[1][0][2], line_list[1][0][4], line_list[1][0][5])
            upper_bound = LinePoint(line_list[2][0][3], line_list[2][0][2], line_list[2][0][4], line_list[2][0][5])
            left_bound = LinePoint(line_list[3][0][3], line_list[3][0][2], line_list[3][0][4], line_list[3][0][5])
            right_bound = LinePoint(line_list[4][0][3], line_list[4][0][2], line_list[4][0][4], line_list[4][0][5])
            list_bounding = [upper_bound, left_bound, right_bound]
            threading.Thread(target=detect_image, args=(
                yolo, 416, YOLO_COCO_CLASSES, 0.4, 0.35, '', True, True,
                camera_id, location, verticalLine, horizontalLine, list_bounding)).start()
            threading.Thread(target=write_images, args=()).start()
            while (cap.isOpened()):
                try:
                    ret, frame = cap.read()
                    count += 1
                    if count % fps == 0:
                        if ret == True:
                            original_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                            original_image = cv2.cvtColor(
                                original_image, cv2.COLOR_BGR2RGB)
                            images_queue.put(original_image)   
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                except:
                    pass
            cap.release()
        else:
            lower_bound = LinePoint(line_list[0][0][3], line_list[0][0][2], line_list[0][0][4], line_list[0][0][5])
            upper_bound = LinePoint(line_list[1][0][3], line_list[1][0][2], line_list[1][0][4], line_list[1][0][5])
            left_bound = LinePoint(line_list[2][0][3], line_list[2][0][2], line_list[2][0][4], line_list[2][0][5])
            right_bound = LinePoint(line_list[3][0][3], line_list[3][0][2], line_list[3][0][4], line_list[3][0][5])
            list_bounding = [upper_bound, left_bound, right_bound]
            threading.Thread(target=detect_image_left, args=(
                yolo, 416, YOLO_COCO_CLASSES, 0.4, 0.35, '', True, True,
                camera_id, location, lower_bound, list_bounding)).start()
            threading.Thread(target=write_images_left, args=()).start()            
            while (cap.isOpened()):
                try:
                    ret, frame = cap.read()
                    count += 1
                    if count % fps == 0:
                        if ret == True:
                            original_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                            original_image = cv2.cvtColor(
                                original_image, cv2.COLOR_BGR2RGB)
                            images_queue_left.put(original_image)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                except:
                    pass
            cap.release()


if __name__ == '__main__':
    list_camera = retrieve_camera()
    for camera in list_camera:
        # start threading
        thread = threading.Thread(target=process_each_frame, args=(camera,))
        #thread.daemon = True
        thread.start()
    while True:
        time.sleep(1)
        clock.tick(30)
        current_time = pygame.time.get_ticks()
        if (current_time - start_time) >= stop_after:
            end_game()
