import os
import threading
import base64
import random
import math
import sys
import copy
import requests

from datetime import datetime
from database_access import *
from service.firebase import save
from service.license_plate_service import check_license_plate
from utils.drawutils import *
from word_ocr import orc_image
from yolov3.configs import *
from yolov3.utils import Load_Yolo_model, detect_image
from service.license_plate_service import check_license_plate


yolo = Load_Yolo_model()


def dist(p1, p2):
    return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) * 2 + (p1.y - p2.y) * (p1.y - p2.y))


def return_enc(motorbike):
    image = motorbike.img_enc
    list_result = detect_image(yolo, image, 416, YOLO_COCO_CLASSES, 0.4, 0.4, '', True, True)
    motorbike_list = list_result['MotorbikeLocation']
    result = motorbike
    min_dis = sys.maxsize
    for moto in motorbike_list:
        dis = dist(motorbike, moto)
        if dis < min_dis:
            min_dis = dis
            result = moto
            print("update distance =", min_dis)
    final_result = copy.copy(motorbike)
    if min_dis < (motorbike.bottom - motorbike.top) * 1.5:
        print("UPDATE ENCROACHING")
        final_result.enc_l = result.left
        final_result.enc_t = result.top
        final_result.enc_r = result.right
        final_result.enc_b = result.bottom
    return final_result


def get_time():
    now = datetime.datetime.now()
    dt_string = now.strftime("%d%m%Y%H%M%S")
    # print("date and time =", dt_string)
    return dt_string


def get_time_v2():
    now = datetime.datetime.now()
    dt_string = now.strftime("%d:%m:%Y %H:%M:%S")
    return dt_string


def save_license_image(img):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\License\\'
    path = 'D:\\TestDTVC\\License\\'

    # os.chdir(path)
    image_name = path + str(get_time()) + str(random.randint(10, 99)) + '.jpg'
    # print("image name = ", image_name)

    cv2.imwrite(image_name, img)


def save_violation_image(img):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Violations\\'
    path = 'D:\\TestDTVC\\Violations\\'

    # os.chdir(path)
    image_name = path + str(get_time()) + str(random.randint(10, 99)) + '.jpg'
    # print("image name = ", image_name)

    cv2.imwrite(image_name, img)


def conduct_case(camera_id, motorbike, location, verticalLine, horizontalLine, list_bounding):
    target_rate = 40.0
    url = "http://127.0.0.1:5000/licence"

    # PASSING RED LIGHT
    if motorbike.vio_pas_rate > 0:
        img_crop = motorbike.img_pas[int(motorbike.pas_t):int(
            motorbike.pas_b), int(motorbike.pas_l):int(motorbike.pas_r)]
        license_plate_location = check_license_plate(img_crop)
        save_license_image(img_crop)
        if len(license_plate_location) != 0:
            violation_id = 1
            file_name = str(violation_id) + str(get_time()) + '.jpg'
            storage_picture = draw_motorbike(
                motorbike.img_pas, motorbike.pas_l, motorbike.pas_t, motorbike.pas_r, motorbike.pas_b)
            storage_picture = draw_line(storage_picture, horizontalLine, False)
            storage_picture = put_Text(
                storage_picture, motorbike.pas_l, motorbike.pas_t, 'passing red light')
            storage_picture = draw_bound(storage_picture, list_bounding)
            save_violation_image(storage_picture)
            cv2.imwrite(file_name, storage_picture)
            image_url = save('1' + file_name, file_name)
            image_id = insert_image(image_url, camera_id)
            # print("co loi passing ne - ", image_id)
            retval, buffer = cv2.imencode('.jpg', img_crop)
            jpg_as_text = base64.b64encode(buffer).decode().replace("'", '"')
            case_id = insert_case(violation_id, location, "", image_id)
            # insert_case_rate(violation_id, location, "license_number", image_id, motorbike.vio_pas_rate)
            myData = {
                'violation_id': case_id,
                'location': location,
                'license_plate_location': license_plate_location,
                'image': jpg_as_text
            }
            requests.post(url, json=myData)
            try:
                os.remove(file_name)
            except:
                pass
        else:
            print("xui ghe passing ko co bien so")

    # CROSSING
    if (motorbike.vio_pas_rate < 0.5) and (motorbike.vio_cro_rate > 45):
        img_crop = motorbike.img_cro[int(motorbike.cro_t):int(
            motorbike.cro_b), int(motorbike.cro_l):int(motorbike.cro_r)]
        license_plate_location = check_license_plate(img_crop)
        if len(license_plate_location) != 0:
            save_license_image(img_crop)
            violation_id = 2
            file_name = str(violation_id) + str(get_time()) + '.jpg'
            storage_picture = draw_motorbike(
                motorbike.img_cro, motorbike.cro_l, motorbike.cro_t, motorbike.cro_r, motorbike.cro_b)
            storage_picture = draw_line(storage_picture, horizontalLine, False)
            storage_picture = put_Text(
                storage_picture, motorbike.cro_l, motorbike.cro_t, 'crossing')
            storage_picture = draw_bound(storage_picture, list_bounding)
            save_violation_image(storage_picture)
            cv2.imwrite(file_name, storage_picture)
            image_url = save('2' + file_name, file_name)
            image_id = insert_image(image_url, camera_id)
            print("co loi crossing ne - ", image_id)
            retval, buffer = cv2.imencode('.jpg', img_crop)
            jpg_as_text = base64.b64encode(buffer).decode().replace("'", '"')
            case_id = insert_case(violation_id, location, "", image_id)
            # insert_case_rate(violation_id, location, "license_number", image_id, motorbike.vio_cro_rate)
            myData = {
                'violation_id': case_id,
                'location': location,
                'license_plate_location': license_plate_location,
                'image': jpg_as_text
            }
            requests.post(url, json=myData)
            try:
                os.remove(file_name)
            except:
                pass
        else:
            print("xui ghe crossing ko co bien so")

    # ENCROACHING
    if motorbike.vio_enc_rate > 30.0:
        motorbike = return_enc(motorbike)
        img_crop = motorbike.img_enc[int(motorbike.enc_t):int(
            motorbike.enc_b), int(motorbike.enc_l):int(motorbike.enc_r)]
        save_license_image(img_crop)            
        license_plate_location = check_license_plate(img_crop)
        if len(license_plate_location) != 0:
            violation_id = 3
            file_name = str(violation_id) + str(get_time()) + '.jpg'
            storage_picture = draw_motorbike(
                motorbike.img_enc, motorbike.enc_l, motorbike.enc_t, motorbike.enc_r, motorbike.enc_b)
            storage_picture = draw_line(storage_picture, verticalLine, True)
            storage_picture = put_Text(
                storage_picture, motorbike.enc_l, motorbike.enc_t, 'encroaching')
            storage_picture = draw_bound(storage_picture, list_bounding)
            save_violation_image(storage_picture)
            cv2.imwrite(file_name, storage_picture)
            image_url = save('3' + file_name, file_name)
            image_id = insert_image(image_url, camera_id)
            print("co loi encroaching ne - ", image_id)
            retval, buffer = cv2.imencode('.jpg', img_crop)
            jpg_as_text = base64.b64encode(buffer).decode().replace("'", '"')
            case_id = insert_case(violation_id, location, "", image_id)
            # insert_case_rate(violation_id, location, "license_number", image_id, motorbike.vio_enc_rate)
            myData = {
                'violation_id': case_id,
                'location': location,
                'license_plate_location': license_plate_location,
                'image': jpg_as_text
            }
            requests.post(url, json=myData)
            try:
                os.remove(file_name)
            except:
                pass
        else:
            print("xui ghe encroaching ko co bien so")

    # NOT WEARING HELMET
    if motorbike.vio_hel_rate > 85.0:
        img_crop = motorbike.img_hel[int(motorbike.hel_t):int(
            motorbike.hel_b), int(motorbike.hel_l):int(motorbike.hel_r)]
        license_plate_location = check_license_plate(img_crop)
        save_license_image(img_crop)
        if len(license_plate_location) != 0:
            violation_id = 4
            file_name = str(violation_id) + str(get_time()) + '.jpg'
            storage_picture = draw_motorbike(
                motorbike.img_hel, motorbike.hel_l, motorbike.hel_t, motorbike.hel_r, motorbike.hel_b)
            storage_picture = put_Text(
                storage_picture, motorbike.hel_l, motorbike.hel_t, 'not wearing helmet')
            storage_picture = draw_bound(storage_picture, list_bounding)
            save_violation_image(storage_picture)
            cv2.imwrite(file_name, storage_picture)
            image_url = save('4' + file_name, file_name)
            image_id = insert_image(image_url, camera_id)
            print("co loi helmet ne - ", image_id)
            retval, buffer = cv2.imencode('.jpg', img_crop)
            jpg_as_text = base64.b64encode(buffer).decode().replace("'", '"')
            case_id = insert_case(violation_id, location, "", image_id)
            # insert_case_rate(violation_id, location, "license_number", image_id, motorbike.vio_hel_rate)
            myData = {
                'violation_id': case_id,
                'location': location,
                'license_plate_location': license_plate_location,
                'image': jpg_as_text
            }
            requests.post(url, json=myData)
            try:
                os.remove(file_name)
            except:
                pass
        else:
            print("xui ghe helmet ko co bien so")

    # return 0


def send_request(url, data):
    try:
        print('Start time for license detection: ' + str(get_time_v2()))
        response = requests.post(url, json=data)
        print(response.text)
        print('Finish time for license detection: ' + str(get_time_v2()))
        print('')
    except:
        print('Request timeout')
        pass


def save_img(count, img, folder):
    path = 'D:\\FPT_Semester\\Capstone_Project\\yolov4-model-server\\imgs\\' + folder + '\\' + str(get_time()) + '-' + str(count) + '.png'
    cv2.imwrite(path, img)
    return path


def conduct_case_v2(camera_id, motorbike, location, verticalLine, horizontalLine, list_bounding):
    url = "http://127.0.0.1:5000/licences"
    # PASSING RED LIGHT
    if motorbike.vio_pas_rate > 0:
        violation_id = 1
        file_name = str(violation_id) + str(get_time()) + '.jpg'
        img = cv2.imread(motorbike.img_pas)
        storage_picture = draw_bbox(image=copy.copy(img), x1=motorbike.pas_l, y1=motorbike.pas_t, x2=motorbike.pas_r, y2=motorbike.pas_b, text='Passing red light')                   
        storage_picture = draw_bound(storage_picture, list_bounding)
        path = save_img(random.randint(1,99), storage_picture, 'firebase')
        image_id = insert_image('', camera_id)
        myData = {
            'violation_id': violation_id,
            'location': location,
            'images': motorbike.imgs,
            'firebase_img': path,
            'img_id': image_id,
            'file_name': file_name
        }
        threading.Thread(target=send_request, args=(url, myData)).start()       

    # CROSSING
    if (motorbike.vio_pas_rate == 0) and (motorbike.vio_cro_rate > 60):
        violation_id = 2
        file_name = str(violation_id) + str(get_time()) + '.jpg'
        img = cv2.imread(motorbike.img_cro)
        storage_picture = draw_bbox(image=copy.copy(img), x1=motorbike.cro_l, y1=motorbike.cro_t, x2=motorbike.cro_r, y2=motorbike.cro_b, text='Crossing')            
        storage_picture = draw_bound(storage_picture, list_bounding)
        path = save_img(random.randint(1,99), storage_picture, 'firebase')

        image_id = insert_image('', camera_id)
        myData = {
            'violation_id': violation_id,
            'location': location,
            'images': motorbike.imgs,
            'firebase_img': path,
            'img_id': image_id,
            'file_name': file_name
        }
        threading.Thread(target=send_request, args=(url, myData)).start()

    # ENCROACHING
    if motorbike.vio_enc_rate > 30.0:
        violation_id = 3
        file_name = str(violation_id) + str(get_time()) + '.jpg'
        img = cv2.imread(motorbike.img_enc)
        storage_picture = draw_bbox(image=copy.copy(img), x1=motorbike.enc_l, y1=motorbike.enc_t, x2=motorbike.enc_r, y2=motorbike.enc_b, text='Encroaching')       
        storage_picture = draw_bound(storage_picture, list_bounding)
        path = save_img(random.randint(1,99), storage_picture, 'firebase')
        image_id = insert_image('', camera_id)
        myData = {
            'violation_id': violation_id,
            'location': location,
            'images': motorbike.imgs,
            'firebase_img': path,
            'img_id': image_id,
            'file_name': file_name
        }
        threading.Thread(target=send_request, args=(url, myData)).start()

    # NOT WEARING HELMET
    if motorbike.vio_hel_rate >= 50.0:
        violation_id = 4
        file_name = str(violation_id) + str(get_time()) + '.jpg'
        img = cv2.imread(motorbike.img_hel)
        storage_picture = draw_bbox(image=copy.copy(img), x1=motorbike.hel_l, y1=motorbike.hel_t, x2=motorbike.hel_r, y2=motorbike.hel_b, text='Not wearing hetmet')
        storage_picture = draw_bound(storage_picture, list_bounding)
        path = save_img(random.randint(1,99), storage_picture, 'firebase')
        image_id = insert_image('', camera_id)
        myData = {
            'violation_id': violation_id,
            'location': location,
            'images': motorbike.imgs,
            'firebase_img': path,
            'img_id': image_id,
            'file_name': file_name
        }
        threading.Thread(target=send_request, args=(url, myData)).start()

    # Wrong lane
    if motorbike.vio_wro_rate > 0:
        violation_id = 5
        file_name = str(violation_id) + str(get_time()) + '.jpg'
        img = cv2.imread(motorbike.img_wro)
        storage_picture = draw_bbox(image=copy.copy(img), x1=motorbike.wro_l, y1=motorbike.wro_t, x2=motorbike.wro_r, y2=motorbike.wro_b, text='Wrong lane')
        storage_picture = draw_bound_left(storage_picture, list_bounding, horizontalLine)
        path = save_img(random.randint(1,99), storage_picture, 'firebase')
        image_id = insert_image('', camera_id)
        myData = {
           'violation_id': violation_id,
            'location': location,
            'images': motorbike.imgs,
            'firebase_img': path,
            'img_id': image_id,
            'file_name': file_name
        }        
        threading.Thread(target=send_request, args=(url, myData)).start()


def draw_motorbike(image, left, top, right, bottom):
    img = copy.copy(image)
    cv2.rectangle(img, (left, top), (right, bottom), (50, 50, 240), 2)
    return img


def draw_object_with_text(image, left, top, right, bottom, text):
    img = copy.copy(image)
    img = cv2.rectangle(img, (left, top), (right, bottom), (50, 50, 240), 2)
    org = (left + 10, top - 15)
    color = (36, 255, 12)
    image = cv2.putText(
        img, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
    return image


def draw_line(image, line, is_verticle):
    img = copy.copy(image)
    start_point = (line.left, line.top)
    end_point = (line.right, line.bottom)
    color = (255, 153, 255)
    if is_verticle:
        color = (0, 255, 0)
    thickness = 8
    if is_verticle:
        thickness = 9
    # Using cv2.line() method
    # Draw a diagonal green line with thickness of 9 px
    final_image = cv2.line(img, start_point, end_point, color, thickness)
    return final_image


def put_Text(inserted_image, left, top, text):
    image = copy.copy(inserted_image)
    org = (left + 10, top - 15)
    color = (50, 50, 240)
    image = cv2.putText(
        image, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
    return image


def draw_bound(inserted_image, list_bounding):
    image = copy.copy(inserted_image)
    color = (255, 0, 0)
    thickness = 3
    for line in list_bounding:
        start_point = (line.left, line.top)
        end_point = (line.right, line.bottom)
        image = cv2.line(image, start_point, end_point, color, thickness)
    return image


def draw_bound_left(inserted_image, list_bounding, lower_bound):
    image = copy.copy(inserted_image)
    color = (255, 0, 0)
    thickness = 2
    iamge = cv2.line(image, (lower_bound.left, lower_bound.top), (lower_bound.right, lower_bound.bottom), color, thickness)
    for line in list_bounding:
        start_point = (line.left, line.top)
        end_point = (line.right, line.bottom)
        image = cv2.line(image, start_point, end_point, color, thickness)
    return image


def draw_bbox(image='', x1=0, y1=0, x2=0, y2=0, color=[50, 50, 240], text=''):
    cv2.rectangle(image, (x1, y1), (x2, y2), color, 2)
    image = cv2.putText(image, text, (x1 - 10, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
    return image

